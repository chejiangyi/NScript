﻿namespace BSF.BaseService.NScript
{
    partial class FrCodeEdit
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrCodeEdit));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.lbInfo = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCompiler = new System.Windows.Forms.ToolStripButton();
            this.btnDebug = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnTemplates = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnTemplate1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnHelp = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnAuthor = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtbCode = new ICSharpCode.TextEditor.TextEditorControl();
            this.lbFileList = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAssemblyDll = new System.Windows.Forms.ToolStripButton();
            this.btnExeHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpen,
            this.toolStripSeparator1,
            this.btnSave,
            this.lbInfo,
            this.toolStripSeparator2,
            this.btnCompiler,
            this.btnDebug,
            this.toolStripSeparator3,
            this.btnAssemblyDll,
            this.btnTemplates,
            this.toolStripSeparator4,
            this.btnHelp});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(682, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnOpen
            // 
            this.btnOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.Image")));
            this.btnOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(36, 22);
            this.btnOpen.Text = "打开";
            this.btnOpen.ToolTipText = "打开Main主文件";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSave
            // 
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(36, 22);
            this.btnSave.Text = "保存";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lbInfo
            // 
            this.lbInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lbInfo.ForeColor = System.Drawing.Color.Green;
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(150, 22);
            this.lbInfo.Text = ".Net脚本编辑器 by 车江毅";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCompiler
            // 
            this.btnCompiler.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnCompiler.Image = ((System.Drawing.Image)(resources.GetObject("btnCompiler.Image")));
            this.btnCompiler.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCompiler.Name = "btnCompiler";
            this.btnCompiler.Size = new System.Drawing.Size(36, 22);
            this.btnCompiler.Text = "编译";
            this.btnCompiler.Click += new System.EventHandler(this.btnCompiler_Click);
            // 
            // btnDebug
            // 
            this.btnDebug.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDebug.Image = ((System.Drawing.Image)(resources.GetObject("btnDebug.Image")));
            this.btnDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.Size = new System.Drawing.Size(36, 22);
            this.btnDebug.Text = "调试";
            this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnTemplates
            // 
            this.btnTemplates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnTemplates.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTemplate1});
            this.btnTemplates.Image = ((System.Drawing.Image)(resources.GetObject("btnTemplates.Image")));
            this.btnTemplates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTemplates.Name = "btnTemplates";
            this.btnTemplates.Size = new System.Drawing.Size(45, 22);
            this.btnTemplates.Text = "模板";
            // 
            // btnTemplate1
            // 
            this.btnTemplate1.Name = "btnTemplate1";
            this.btnTemplate1.Size = new System.Drawing.Size(152, 22);
            this.btnTemplate1.Text = "模板1";
            this.btnTemplate1.Click += new System.EventHandler(this.btnTemplate1_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAuthor,
            this.btnExeHelp});
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(45, 22);
            this.btnHelp.Text = "帮助";
            // 
            // btnAuthor
            // 
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(152, 22);
            this.btnAuthor.Text = "作者";
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(682, 481);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rtbCode);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lbFileList);
            this.splitContainer1.Size = new System.Drawing.Size(682, 481);
            this.splitContainer1.SplitterDistance = 550;
            this.splitContainer1.TabIndex = 0;
            // 
            // rtbCode
            // 
            this.rtbCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCode.IsReadOnly = false;
            this.rtbCode.Location = new System.Drawing.Point(0, 0);
            this.rtbCode.Name = "rtbCode";
            this.rtbCode.Size = new System.Drawing.Size(550, 481);
            this.rtbCode.TabIndex = 0;
            // 
            // lbFileList
            // 
            this.lbFileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbFileList.FormattingEnabled = true;
            this.lbFileList.ItemHeight = 12;
            this.lbFileList.Location = new System.Drawing.Point(0, 0);
            this.lbFileList.Name = "lbFileList";
            this.lbFileList.Size = new System.Drawing.Size(128, 481);
            this.lbFileList.TabIndex = 0;
            this.lbFileList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbFileList_MouseDoubleClick);
            this.lbFileList.MouseHover += new System.EventHandler(this.lbFileList_MouseHover);
            this.lbFileList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lbFileList_MouseMove);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 3000;
            this.toolTip1.InitialDelay = 3000;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 3000;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAssemblyDll
            // 
            this.btnAssemblyDll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnAssemblyDll.Image = ((System.Drawing.Image)(resources.GetObject("btnAssemblyDll.Image")));
            this.btnAssemblyDll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAssemblyDll.Name = "btnAssemblyDll";
            this.btnAssemblyDll.Size = new System.Drawing.Size(36, 22);
            this.btnAssemblyDll.Text = "引用";
            this.btnAssemblyDll.ToolTipText = "需要引用的dll的情况";
            this.btnAssemblyDll.Click += new System.EventHandler(this.btnAssemblyDll_Click);
            // 
            // btnExeHelp
            // 
            this.btnExeHelp.Name = "btnExeHelp";
            this.btnExeHelp.Size = new System.Drawing.Size(152, 22);
            this.btnExeHelp.Text = "说明";
            this.btnExeHelp.Click += new System.EventHandler(this.btnExeHelp_Click);
            // 
            // FrCodeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(682, 506);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FrCodeEdit";
            this.Text = "NScript Editor (.Net 脚本编辑器)";
            this.Load += new System.EventHandler(this.FrCodeEdit_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnOpen;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox lbFileList;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripLabel lbInfo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnDebug;
        private System.Windows.Forms.ToolStripButton btnCompiler;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripDropDownButton btnTemplates;
        private System.Windows.Forms.ToolStripMenuItem btnTemplate1;
        private ICSharpCode.TextEditor.TextEditorControl rtbCode;
        private System.Windows.Forms.ToolStripDropDownButton btnHelp;
        private System.Windows.Forms.ToolStripMenuItem btnAuthor;
        private System.Windows.Forms.ToolStripButton btnAssemblyDll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnExeHelp;

    }
}

